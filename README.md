Query Postgres General
=========

This role can be used by any playbook to query anything from the postgresql db.


Requirements
------------
Just make sure the inventory you use in the Ansible tower has the needed parameters to login to the db.
And of course the query you define before calling the role has to be correctly set by the syntax of the postgres_query for ansible.
See more examples at https://docs.ansible.com/ansible/latest/collections/community/general/postgresql_query_module.html

Role Variables
--------------

The variables mentioned here are either in the inventory, assigned at the playbook that calls the role and those that are defined by the role itself

| Variable                | Required | Default | Choices                   | Comments                             |
|-------------------------|----------|---------|---------------------------|--------------------------------------|
| db_host                 | yes      |         |                           | cloudlet-postgressql.some-domain.com |
| db_user                 | yes      |         |                           | postgres-user                        |
| db_pass                 | yes      |         |                           | postgres-pass                        |
| query                   | yes      |         |                           | SELECT * FROM cloudlets              |
| query_results           |          | depends on the query        |       | some json                            |


Example Playbook
----------------

You can see the fool playbook here: https://gitlab.com/ananon/ansible/delete-test-cluster-from-postgres.
It may serve as a good example because it calls the role several times, each time defining the query differently and also handling the results with json module.

```yaml
---
- name: delete test cluster from postgres db
  hosts: localhost
  become: false
  gather_facts: false
  tasks:
    - name: create query get id
      set_fact:
        query: SELECT id FROM cloudlets WHERE name = '{{ cluster_name }}'

    - name: call role to get id
      include_role:
        name: role-query-postgres-general
```